<!DOCTYPE html>
<html <?php language_attributes() ?> class="no-js">
<head>
    <meta charset="<?php bloginfo('charset') ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title><?php wp_title() ?></title>
    <link rel="profile" href="http://gmpg.org/xfn/11">
    <?php wp_head() ?>
    <?php get_template_part('parts/header', 'bootstrap') ?>
</head>
<body <?php body_class() ?>>
<?php get_template_part('parts/header', 'header');

<?php $timestamp = get_post_time('U', true) ?>
<article class="post">
    <div class="container">
        <div class="row">

            <div class="col-sm-12 wide empty"></div>

            <div class="col-sm-4 text-right meta">

                <h2 class="post-title"><?php the_title() ?></h2>
                <div class="post-date"><?php echo date_i18n(get_option('date_format'), $timestamp) ?></div>
                <div class="post-time"><?php echo date_i18n(get_option('time_format'), $timestamp) ?></div>

                <div class="post-comments">
                    <a href="<?php the_permalink() ?>">
                        <?php _e('Reacties', 'retrospace') ?>
                    </a>
                </div>

            </div>

            <div class="col-sm-8 excerpt">
                <?php the_content() ?>
            </div>

        </div>
    </div>
</article>

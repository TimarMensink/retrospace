<?php if (function_exists('yoast_breadcrumb')) : ?>

    <div id="breadcrumbs" class="container">
        <?php yoast_breadcrumb() ?>
    </div>

<?php endif;

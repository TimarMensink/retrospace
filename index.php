<?php get_header() ?>

<main id="page">

    <?php if (have_posts()) : ?>

        <?php if (!is_home()) : ?>

            <?php get_template_part('parts/structure', 'breadcrumbs') ?>

        <?php endif; ?>

        <?php while (have_posts()) : the_post(); ?>

            <?php get_template_part('parts/loop', 'post') ?>

        <?php endwhile; ?>

    <?php else : ?>

        <section class="error is_404">

            <h1><?php _e('404: Pagina niet gevonden', 'retrospace') ?></h1>

            <p><?php _e('Deze pagina kon niet worden gevonden', 'retrospace') ?></p>

        </section>

    <?php endif; ?>

</main>

<?php get_footer();

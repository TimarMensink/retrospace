<?php
/**
 *  This is not the file you are looking for
 */

$files = ( defined( 'WP_DEBUG' ) AND WP_DEBUG )
    ? glob( dirname( __FILE__ ) . '/assets/functions/*.php', GLOB_ERR )
    : glob( dirname( __FILE__ ) . '/assets/functions/*.php' );

foreach ( $files as $file )
    include $file;

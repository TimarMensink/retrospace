(function($){

    // as soon as possible
    $('html.no-js').removeClass('no-js');

    // save to manipulate DOM
    $(document).ready(function($) {
        /**
         * Posts
         */
        // if post title is wider than the meta box, make it as wide as the whole post
        $('article.post .post-title').each(function(){
            if ($(this)[0].scrollWidth-30 > $(this).innerWidth()) {
                var target = $(this).parents('article.post').find('.wide');
                $(this).appendTo(target);
                console.log('bloep')
            }
        });
    });

})(jQuery);

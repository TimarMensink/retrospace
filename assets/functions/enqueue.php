<?php
/**
 * Enqueue styles & scripts
 */

add_action( 'wp_enqueue_scripts', 'retrospace_enqueue' );

function retrospace_enqueue() {
//    global $wp_styles;

    // Load the main stylesheet
    wp_enqueue_style('retrospace-css', get_stylesheet_uri());
    wp_enqueue_script('retrospace-js', get_stylesheet_directory_uri() . '/script.js', array('jquery'));

    /**
     * Load our IE-only stylesheet for all versions of IE:
     * <!--[if IE]> ... <![endif]-->
     *
     * NOTE: It is also possible to just check and see if the $is_IE global in WordPress is set to true before
     * calling the wp_enqueue_style() function.  If you are trying to load a stylesheet for all browsers
     * EXCEPT for IE, then you would HAVE to check the $is_IE global since WordPress doesn't have a way to
     * properly handle non-IE conditional comments.
     */
//    wp_enqueue_style( 'my-theme-ie', get_stylesheet_directory_uri() . "/css/ie.css", array( 'my-theme' )  );
//    $wp_styles->add_data( 'my-theme-ie', 'conditional', 'IE' );
}
